# 繁历

重生之我是全干骡子

## 经验

+ 中国电建集团昆明科研勘探设计院
  + 参与昆明市智慧水务系统的前后端应用开发.
  + 参与云南省滇中引水工程 “ 智慧滇中引水 ” 建设管理信息系统（一期）的前后端应用开发.
+ 开源社区
  + 修复 [YesPlayMusic](https://github.com/qier222/YesPlayMusic) 软件的一个[重大 bug](https://github.com/qier222/YesPlayMusic/issues/1019), 还有其他的一些小bug.
  + 参与过 [style2paints](https://github.com/lllyasviel/style2paints) 的早期开发、测试工作 (连 logo 都是我做的((((().
  + 参与过 style2paints 作者的另一个项目 [PaintingLight](https://github.com/lllyasviel/PaintingLight), 以及[MangaCraft](https://github.com/lllyasviel/MangaCraft) (现已将功能合并到 s2p 所以删库跑路了).
  + 参与过 DeepCreamPy 的测试工作.
  + 参与 [VRoid Studio 翻译](https://github.com/xiaoye97/VRoidChinese)工作.
  + FreeBSD 中文社区成员, 参与过文档攥写和翻译.
  + 做了其他的一些根本没人看的项目.

## 技术栈

+ 熟悉使用 vue 框架和 spring 全家桶的 web 单页面应用开发.
  + 了解 electron 框架下的桌面/移动应用开发.
+ 熟悉 Linux 系统为主的服务器搭建、运维、内网、外网通信设备、堡垒机、跳板机的搭建、运维、Gitlab 的搭建、运维.
+ 熟悉 Hadoop 生态的的系统、应用部署、运维.
+ 熟悉 MySQL、PostgreSQL、Redis、MongoDB、HDFS 的使用、运维.
+ 了解 Tensorflow 框架下的图形识别技术设计与实现
  + 了解 CUDA 框架、CUDNN 加速计算技术、光线追踪的设计原理.
  + 了解各类动作捕捉技术的使用.
+ 了解各类图形api、3D建模软件、渲染软件、引擎的使用.
+ 了解计算机原理, 熟悉 Windows/MacOS/Linux 系统的使用.
  + 了解 Windows NT 内核原理.
  + 了解 NTFS、ext、ZFS、FAT 等文件系统原理.
+ 了解 Java 虚拟机原理与优化.
+ 了解图形设计, 制图等.

<details>
  <summary>其他技能</summary>

+ 熟练掌握数据挖掘/信息检索技能.
+ 掌握 Office 全家桶 adobe 全家桶的使用.
+ 熟练使用 git .
+ 掌握文档攥写技能.
+ 掌握英语语言.
+ 会画画.

</details>
